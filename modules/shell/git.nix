{ ... }:

{
  hm.programs.git = {
    enable = true;

    lfs.enable = true;

    aliases = { adog = "log --abbrev --decorate --oneline --graph"; };

    extraConfig = {
      pull.rebase = true;
      init.defaultBranch = "main";
    };

    signing = {
      key = "CD515D5D1808384B";
      signByDefault = true;
    };

    userName = "aalbert";
    userEmail = "augustin.albert@bleu-azure.fr";
  };
}
