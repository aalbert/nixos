{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    bat
    tree
    wget
    htop
    vim
    ripgrep
    fd
  ];
}
