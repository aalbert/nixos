{ pkgs, ... }:

{
  hm.programs.gpg = {
    enable = true;

    settings = {
      expert = true;
      keyserver = "keyserver.ubuntu.com";
    };
  };

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };
}

