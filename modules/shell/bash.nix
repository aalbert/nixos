{ pkgs, ... }:

{
  hm.programs.bash = {
    enable = true;

    historyIgnore = [ "fg" ];

    sessionVariables = {
      EDITOR = "hx";
    };

    shellOptions = [ "autocd" ];

    shellAliases = {
      emacs = "emacs -nw";
      reducepdf = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output.pdf ";
    };
  };
}
