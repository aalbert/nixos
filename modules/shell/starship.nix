{ lib, pkgs, ... }:

{
  hm.programs.bash = {
    initExtra = ''
      eval "$(${pkgs.starship}/bin/starship init bash)"
    '';
  };

  hm.programs.starship = {
    enable = true;

    settings = {
        add_newline = true;

        format = lib.concatStrings [
          "$username"
          "$hostname"
          "$directory"
          "$git_branch"
          "$git_commit"
          "$git_state"
          "$git_status"
          "$memory_usage"
          "$nix_shell"
          "$fill"
          "$cmd_duration"
          "$time"
          "$line_break"
          "$jobs"
          "$character"""
        ];

        memory_usage = {
          disabled = false;
          threshold = 75;
          style = "bold dimmed white";
          format = "RAM [$ram]($style)";
        };

        time = {
          disabled = false;
          format = ''[\($time\)]($style) '';
          time_format = "%Hh%M";
          utc_time_offset = "+2";
        };

        fill = {
          style = "bold dimmed white";
          symbol = "·";
        };
    };
  };
}
