
{ config, pkgs, ... }:

{
  user.packages = with pkgs; [
    texlive.combined.scheme-full
    typst
    tinymist
    typstyle
    bibtex-tidy
  ];

  fonts.packages = with pkgs; [
    noto-fonts-cjk-sans
    fira-math
    fira-code
  ];
}
