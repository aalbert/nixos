{ pkgs, ... }:

{
  hm.programs.librewolf = {
    enable = true;
    package = pkgs.librewolf-wayland;
  };
}
