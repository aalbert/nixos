{ inputs, lib, ... }:

with inputs.home-manager.lib.hm.gvariant;

let
  background = builtins.fetchurl {
    url = "https://raw.githubusercontent.com/NixOS/nixos-artwork/master/wallpapers/nix-wallpaper-nineish.src.svg";
    sha256 = "sha256:059hjcnpy7jj8bijs2xbjmwfc41dxy4pl801nkhblrdxiny21s0h";
  };

  background-dark = builtins.fetchurl {
    url = "https://raw.githubusercontent.com/NixOS/nixos-artwork/master/wallpapers/nix-wallpaper-nineish-dark-gray.svg";
    sha256 = "sha256:0qrl6lmj2ss77f3dkdc9arqr45w5p26krb1rjgvvgbwzcp4qrvdg";
  };
in
{
  hm.dconf.settings = {

    "org/freedesktop/tracker/miner/files" = {
      index-single-directories = [ ];
      index-recursive-directories = [ ];
    };

    "org/gnome/Console" = {
      theme = "auto";
      zoom = 1.0;
    };

    "org/gnome/desktop/background" = {
      picture-uri = "${background}";
      picture-uri-dark = "${background-dark}";
    };

    "org/gnome/desktop/input-sources" = {
      sources = [
        (mkTuple [
          "xkb"
          "fr+azerty"
        ])
      ];
    };

    "org/gnome/desktop/interface" = {
      clock-show-weekday = true;
      enable-hot-corners = false;
      show-battery-percentage = true;
      text-scaling-factor = 1.25;
    };

    "org/gnome/desktop/notifications/application/org-gnome-console" = {
      enable = false;
      enable-sound-alerts = false;
    };

    "org/gnome/desktop/peripherals/keyboard" = {
      delay = mkUint32 230;
      repeat-interval = mkUint32 22;
    };

    "org/gnome/desktop/peripherals/mouse" = {
      natural-scroll = false;
    };

    "org/gnome/desktop/peripherals/touchpad" = {
      natural-scroll = false;
      tap-to-click = true;
    };

    "org/gnome/desktop/session" = {
      idle-delay = mkUint32 0;
    };

    "org/gnome/desktop/wm/keybindings" = {
      close = [ "<Super><Shift>q" ];
      toggle-fullscreen = [ "<Super>f" ];

      maximize = [ ];
      minimize = [ ];
      unmaximize = [ ];

      move-to-workspace-1 = [ "<Super><Shift>1" ];
      move-to-workspace-2 = [ "<Super><Shift>2" ];
      move-to-workspace-3 = [ "<Super><Shift>3" ];
      move-to-workspace-4 = [ "<Super><Shift>4" ];
      move-to-workspace-5 = [ "<Super><Shift>5" ];
      move-to-workspace-6 = [ "<Super><Shift>6" ];
      move-to-workspace-7 = [ "<Super><Shift>7" ];
      move-to-workspace-8 = [ "<Super><Shift>8" ];
      move-to-workspace-9 = [ "<Super><Shift>9" ];
      move-to-workspace-10 = [ "<Super><Shift>0" ];

      switch-input-source = [ "<Super>i" ];
      switch-input-source-backward = [ "<Shift><Super>i" ];

      switch-to-workspace-1 = [ "<Super>1" ];
      switch-to-workspace-2 = [ "<Super>2" ];
      switch-to-workspace-3 = [ "<Super>3" ];
      switch-to-workspace-4 = [ "<Super>4" ];
      switch-to-workspace-5 = [ "<Super>5" ];
      switch-to-workspace-6 = [ "<Super>6" ];
      switch-to-workspace-7 = [ "<Super>7" ];
      switch-to-workspace-8 = [ "<Super>8" ];
      switch-to-workspace-9 = [ "<Super>9" ];
      switch-to-workspace-10 = [ "<Super>0" ];
    };

    "org/gnome/desktop/wm/preferences" = {
      audible-bell = false;
      button-layout = "appmenu";
      focus-mode = "mouse";
      #num-workspaces = 6;
    };

    "org/gnome/mutter" = {
      dynamic-workspaces = true;
      edge-tiling = false;
      workspaces-only-on-primary = true;
    };

    "org/gnome/settings-daemon/plugins/media-keys" = {
      custom-keybindings = [
        "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/"
        "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/"
        "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/"
      ];
      screensaver = [ "<Super><Shift>l" ];
    };

    "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" = {
      binding = "<Super>b";
      command = "librewolf";
      name = "Browser";
    };

    "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1" = {
      binding = "<Super>z";
      command = "kgx";
      name = "Terminal";
    };

    "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2" = {
      binding = "<Super>a";
      command = "nautilus";
      name = "Files";
    };

    "org/gnome/settings-daemon/plugins/power" = {
      idle-dim = true;
      power-button-action = "suspend";
      sleep-inactive-ac-type = "nothing";
      sleep-inactive-battery-type = "nothing";
    };

    #"org/gnome/shell" = { disable-extension-version-validation = true; };

    "org/gnome/shell/keybindings" = {
      switch-to-application-1 = [ ];
      switch-to-application-2 = [ ];
      switch-to-application-3 = [ ];
      switch-to-application-4 = [ ];
      switch-to-application-5 = [ ];
      switch-to-application-6 = [ ];
      switch-to-application-7 = [ ];
      switch-to-application-8 = [ ];
      switch-to-application-9 = [ ];

      toggle-message-tray = [ "<Super>V" ];
      toggle-application-view = [ ];
      toggle-overview = [ ];
    };

  };
}
