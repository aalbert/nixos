{ pkgs, lib, ... }:

{
  options.gnome = with lib; {
    autocommands =
      let
        f =
          w:
          mkOption {
            type = with types; listOf str;
            default = [ ];
            description = "Commands to execute on ${w}";
          };
      in
      {
        sunrise = f "sunrise";
        sunset = f "sunset";
      };
  };

  config = {

    services = {
      gnome.core-utilities.enable = false;
      gnome.tinysparql.enable = false;

      xserver = {
        enable = true;
        displayManager.gdm = {
          enable = true;
          wayland = true;
        };
        desktopManager.gnome.enable = true;
        xkb.layout = "fr";
        xkb.variant = "azerty";
      };
    };

    user.packages = with pkgs; [
      papers # evince replacement
      eog
      nautilus
      baobab
      gnome-console
      gnome-keyring
      gnome-calculator
    ];
  };
}
