{ config
, inputs
, pkgs
, lib
, ...
}:

with inputs.home-manager.lib.hm.gvariant;

let

  autocommands = config.gnome.autocommands;
  mergeCommands =
    name: commands:
    pkgs.writeShellScript "gnome-commands-${name}" (
      lib.concatMapStrings
        (command: ''
          (
            ${command}
          )
        '')
        commands
    );

in

{
  environment.systemPackages = with pkgs.gnomeExtensions; [
    appindicator
    night-theme-switcher
    paperwm
  ];

  hm.dconf.settings = {
    "org/gnome/shell" = {
      disable-user-extensions = false;
      disabled-extensions = [
        # "screenshot-window-sizer@gnome-shell-extensions.gcampax.github.com"
        # "places-menu@gnome-shell-extensions.gcampax.github.com"
        # "windowsNavigator@gnome-shell-extensions.gcampax.github.com"
        # "simulate-switching-workspaces-on-active-monitor@micheledaros.com"
        # "window-list@gnome-shell-extensions.gcampax.github.com"
        # "workspace-indicator@gnome-shell-extensions.gcampax.github.com"
        # "native-window-placement@gnome-shell-extensions.gcampax.github.com"
      ];
      enabled-extensions = [
        "drive-menu@gnome-shell-extensions.gcampax.github.com"
        "launch-new-instance@gnome-shell-extensions.gcampax.github.com"
        "paperwm@paperwm.github.com"
        "appindicatorsupport@rgcjonas.gmail.com"
        "nightthemeswitcher@romainvigier.fr"
      ];
    };

    "org/gnome/shell/extensions/nightthemeswitcher/time" = {
      always-enable-ondemand = true;
      manual-time-source = true;
      ondemand-button-placement = "panel";
      nightthemeswitcher-ondemand-keybinding = [ "<Shift><Super>t" ];
      time-source = "ondemand";
      transition = true;
    };

    "org/gnome/shell/extensions/nightthemeswitcher/commands" = {
      enabled = true;
      sunrise = "${mergeCommands "sunrise" autocommands.sunrise}";
      sunset = "${mergeCommands "sunset" autocommands.sunset}";
    };

    "org/gnome/shell/extensions/paperwm" = {
      horizontal-margin = 0;
      vertical-margin = 0;
      vertical-margin-bottom = 0;
      window-gap = 0;
      #default-show-top-bar = false;
      #show-top-bar = false;
      #show-window-position-bar = false;
      #show-position-bar = false;
    };

  };
}
