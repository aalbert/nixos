{ pkgs, ... }:

{
  user.packages = with pkgs; [
    xclip
    mpv
    gimp
    pdfarranger
    libreoffice
    nextcloud-client
    thunderbird
    signal-desktop
    hunspell
    hunspellDicts.fr-any
    hunspellDicts.en-us
    languagetool
    #python312Packages.grammalecte
    wineWowPackages.stable
    steam-run
    appimage-run
    xournal
    chromium
    ghostscript
    #gramps
  ];
}
