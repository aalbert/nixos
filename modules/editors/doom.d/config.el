;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

(setq user-full-name "Augustin Albert"
      user-mail-address "augustin.albert@bleu-azure.fr")

(setq doom-theme 'doom-one-light)

(setq org-directory "~/org/")

(setq display-line-numbers-type t)

(setq undo-limit 80000000
      evil-want-fine-undo t
      auto-save-default t)

(setq! evil-toggle-key "C-M-z")

(setq org-latex-pdf-process '("latexmk -f -pdf -%latex -shell-escape -interaction=nonstopmode -output-directory=%o %f"))
(setq org-latex-listings 'minted)
(setq org-latex-minted-options
      '(("escapeinside" "♯♯") ("mathescape")))

(load! "+theme.el")

(use-package! typst-ts-mode)

;; (defun add-electric-pair-dollar ()
;;   "Add $ to electric pairs."
;;   (setq-local electric-pair-pairs (append electric-pair-pairs '((?\$ . ?\$))))
;;   (setq-local electric-pair-text-pairs electric-pair-pairs)
;;   (setq-local electric-pair-mode t))
;; (add-hook 'typst-mode-hook #'add-electric-pair-dollar)
