{ config, lib, ... }:

let

  adaptiveFilename = "adaptive";
  themeFolder = "$HOME/.config/helix/themes";
  changeTheme = theme: ''
    mkdir -p "${themeFolder}"
    echo 'inherits = "${theme}"' >"${themeFolder}/${adaptiveFilename}.toml";
    pkill -USR1 hx
  '';

in

{

  config = {
    gnome.autocommands = {
      sunrise = [ (changeTheme "github_light") ];
      sunset = [ (changeTheme "github_dark_dimmed") ];
    };

    hm.programs.helix = {
      enable = true;

      settings = {
        theme = adaptiveFilename;
        editor = {
          #lsp.display-messages = true;
          soft-wrap.enable = true;
          cursorline = true;
          cursor-shape = {
            insert = "bar";
            select = "underline";
          };
        };

        keys.normal = {
          "ret" = ":write";
          "C-o" = ":open ~/";
          "C-p" = ":sh papers *.pdf";
        };
      };

      languages =
        {
          language-server.tinymist = {
            command = "tinymist";
            config = {
              # typstExtraArgs = [ "main.typ" ];
              exportPdf = "onSave";
              outputPath = "$root/$dir/$name";
            };
          };

          language = [
            {
              name = "typst";
              language-servers = [ "tinymist" ];
              formatter.command = "typstyle";
              auto-format = true;
            }
            {
              name = "nix";
              formatter.command = "nixpkgs-fmt";
              auto-format = true;
            }
          ];
        };
    };
  };
}
