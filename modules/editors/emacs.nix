{
  inputs,
  lib,
  pkgs,
  ...
}:

{
  #nixpkgs.overlays = [ inputs.emacs-overlay.overlay ];

  hm = {
    home.file.".doom.d" = {
      recursive = true;
      source = ./doom.d;
    };

    #services.emacs.enable = true;

    programs.emacs = {
      enable = true;
      package = pkgs.emacs;
      extraPackages = (epkgs: [ epkgs.vterm ]);
    };
  };
}
