{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # emacs-overlay = {
    #   url = "github:nix-community/emacs-overlay/master";
    #   inputs.nixpkgs.follows = "nixpkgs";
    # };

  };

  outputs =
    { self, nixpkgs, ... }@inputs:
    let
      lib = nixpkgs.lib.extend (_: prev: { my = import ./lib.nix { lib = prev; }; });
      system = "x86_64-linux";
    in
    {
      nixosConfigurations = lib.my.mapHosts ./hosts { inherit inputs lib system; };
    };
}
