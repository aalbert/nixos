{
  config,
  inputs,
  lib,
  pkgs,
  options,
  ...
}:

with lib.my;

{
  imports = [
    inputs.home-manager.nixosModules.home-manager
  ] ++ (mapModulesRec (toString ./modules) import);

  options = {
    hm = mkOpt (lib.types.attrs) { };
    user = mkOpt (lib.types.attrs) { };
  };

  config = {
    home-manager.users.aalbert = lib.mkAliasDefinitions options.hm;
    users.users.aalbert = lib.mkAliasDefinitions options.user;

    environment.systemPackages = with pkgs; [
      exfat
      ntfs3g
      sshfs
    ];

    system.stateVersion = "22.11";
    hm.home.stateVersion = "22.11";

    nix =
      let
        filteredInputs = lib.filterAttrs (n: _: n != "self") inputs;
        registryInputs = lib.mapAttrs (_: v: { flake = v; }) filteredInputs;
      in
      {
        #package = pkgs.nixUnstable;
        registry = registryInputs // {
          dotfiles.flake = inputs.self;
        };

        settings = {
          experimental-features = [
            "nix-command"
            "flakes"
          ];
          substituters = [ "https://nix-community.cachix.org/" ];
          trusted-public-keys = [ "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs=" ];
        };
      };

    nixpkgs.config.allowUnfree = true;

    time.timeZone = "Europe/Paris";

# Internationalisation properties.
  i18n.defaultLocale = "fr_FR.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "fr_FR.UTF-8";
    LC_IDENTIFICATION = "fr_FR.UTF-8";
    LC_MEASUREMENT = "fr_FR.UTF-8";
    LC_MONETARY = "fr_FR.UTF-8";
    LC_NAME = "fr_FR.UTF-8";
    LC_NUMERIC = "fr_FR.UTF-8";
    LC_PAPER = "fr_FR.UTF-8";
    LC_TELEPHONE = "fr_FR.UTF-8";
    LC_TIME = "fr_FR.UTF-8";
  };

  # Configure console keymap
  console.keyMap = "fr";

    user = {
      createHome = true;
      description = "Augustin Albert";
      extraGroups = [
        "wheel"
        "networkmanager"
      ];
      home = "/home/aalbert";
      isNormalUser = true;
      hashedPassword = "$6$lxp/AuHggs3eGob1$6.Bczhd/aD/tb4q3PwzMvdWY7x0AESXKAHpUTYHqO8ZjTw.H7Dw3hBwEL95hOWOwV9Oc2Wd.CZIE7ooO4Uma50";
      uid = 1000;
    };

    users.mutableUsers = false;
    users.users.root.hashedPassword = "*";
  };
}
